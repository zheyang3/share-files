import torch
import torch.nn as nn
import torch.utils.data
import torchvision
import torchvision.transforms as transforms
import time
from model import ResNet
from model import BasicBlock


# We provide the code for loading CIFAR100 data
num_epochs = 30
batch_size = 128
learning_rate = 0.001


scheduler_step_size = 10
scheduler_gamma = 0.1

# torch.manual_seed(0)
transform_train = transforms.Compose([
    transforms.RandomCrop(32, padding=4),
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])

transform_test = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
])


trainset = torchvision.datasets.CIFAR100(root='./', train=True, download=True, transform=transform_train)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=8)

testset = torchvision.datasets.CIFAR100(root='./', train=False, download=True, transform=transform_test)
testloader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=8)




device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


model = ResNet(BasicBlock, [2,4,4,2], 100).cuda()
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=scheduler_step_size, gamma=scheduler_gamma)


total_step = len(trainloader)
start_time = time.time()

for epoch in range(num_epochs):
    scheduler.step()
    correct = 0
    total = 0
    for images, labels in trainloader:
        # Move tensors to the configured device
        # images = images.reshape(-1, 28*28).to(device)
        images = images.to(device)
        labels = labels.to(device)

        # Forward pass
        outputs = model(images)
        loss = criterion(outputs, labels)


        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    train_accuracy = correct*1.0/total

    with torch.no_grad():
        correct = 0
        total = 0
        for images, labels in testloader:
            # images = images.reshape(-1, 28*28).to(device)
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    test_accuracy = correct*1.0/total


    print ('Epoch {}, Time {:.4f}, Loss: {:.4f}, Train Accuracy: {:.4f},Test Accuracy: {:.4f}'
           .format(epoch, time.time()-start_time, loss.item(),train_accuracy,test_accuracy))
    torch.save(model.state_dict(), 'epoch-{}.ckpt'.format(epoch))
