import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

import time

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# device = torch.device('cpu')
print(device)

num_epochs = 30
batch_size = 100
learning_rate = 0.001


scheduler_step_size = 10
scheduler_gamma = 0.1

transform_train = transforms.Compose([
                    transforms.ToTensor(),
                    transforms.RandomCrop(32, pad_if_needed=True),
                    transforms.RandomHorizontalFlip(),
                    # transforms.RandomPerspective(),
                    transforms.RandomRotation(180)
                  ])

# MNIST dataset 
train_dataset = torchvision.datasets.CIFAR10(root='./',
                                           train=True,
                                           transform=transforms.ToTensor(),
                                           download=True)

test_dataset = torchvision.datasets.CIFAR10(root='./',
                                          train=False,
                                          transform=transforms.ToTensor())

train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=batch_size,
                                          shuffle=False)


from model import NeuralNet

model = NeuralNet().to(device)

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=scheduler_step_size, gamma=scheduler_gamma)


total_step = len(train_loader)
start_time = time.time()
for epoch in range(num_epochs):
    scheduler.step()
    correct = 0
    total = 0
    for images, labels in train_loader:
        # Move tensors to the configured device
        # images = images.reshape(-1, 28*28).to(device)
        images = images.to(device)
        labels = labels.to(device)

        # Forward pass
        outputs = model(images)
        loss = criterion(outputs, labels)


        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum().item()

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    train_accuracy = correct*1.0/total

    with torch.no_grad():
        correct = 0
        total = 0
        for images, labels in test_loader:
            # images = images.reshape(-1, 28*28).to(device)
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    test_accuracy = correct*1.0/total


    print ('Epoch {}, Time {:.4f}, Loss: {:.4f}, Train Accuracy: {:.4f},Test Accuracy: {:.4f}'
           .format(epoch, time.time()-start_time, loss.item(),train_accuracy,test_accuracy))
    torch.save(model.state_dict(), 'epoch-{}.ckpt'.format(epoch))

