import torch
import torch.nn as nn


class NeuralNet(nn.Module):
    def __init__(self, num_classes=10):
        super(NeuralNet, self).__init__()
        self.conv  = nn.Sequential(
            # nn.Conv2d(3, 64, 5, padding=2),                # 1st layer
            nn.Conv2d(3, 128, 5, padding=2),                # 1st layer
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),
            
            # nn.Conv2d(64, 64, 5, padding=2),               # 2nd layer
            nn.Conv2d(128, 128, 5, padding=2),               # 2nd layer
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Dropout2d(p=0.1),

            # nn.Conv2d(64, 64, 5, padding=2),               # 3rd layer
            # nn.Conv2d(128, 64, 5, padding=2),               # 3rd layer
            # nn.ReLU(inplace=True),
            # nn.BatchNorm2d(64),

            nn.Conv2d(128, 256, 5, padding=2),               # 4th layer
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Dropout2d(p=0.1),
            
            ## nn.Conv2d(64, 64, 5, padding=2),               # 5th layer
            ## nn.ReLU(inplace=True),
            ## nn.BatchNorm2d(64),
            
            nn.Conv2d(256, 256, 5, padding=2),               # 6th layer   -2
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),
            nn.MaxPool2d(kernel_size=2, stride=2),
            nn.Dropout2d(p=0.1),
            
            ## nn.Conv2d(64, 64, 3, padding=0),               # 7th layer   -2
            ## nn.ReLU(inplace=True),
            ## nn.BatchNorm2d(64),
            
            # nn.Conv2d(128, 128, 3, padding=0),               # 8th layer   -2
            # nn.ReLU(inplace=True),
            # nn.BatchNorm2d(128),
            # nn.Dropout2d(p=0.1),
            )
        
        self.fc = nn.Sequential(
            nn.Linear(4096, 1000),
            nn.ReLU(inplace=True),
            nn.Linear(1000, 500),
            nn.ReLU(inplace=True),
            nn.Linear(500, num_classes)
            )



    def forward(self, x):
        out = self.conv(x)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out
